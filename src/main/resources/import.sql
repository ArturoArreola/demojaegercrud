/* Populate tabla clientes */
INSERT INTO clientes (nombre, apellido, email, create_at) VALUES('Luis', 'Barragan', 'luis.barragan@coppel.com', '2020-01-02');
INSERT INTO clientes (nombre, apellido, email, create_at) VALUES('Yadira', 'Morales', 'yadira.morales@coppel.com', '2020-01-03');
INSERT INTO clientes (nombre, apellido, email, create_at) VALUES('Edgar', 'Manjarrez', 'edgar.manjarrez@coppel.com', '2020-01-04');
INSERT INTO clientes (nombre, apellido, email, create_at) VALUES('Hector', 'Rivera', 'hector.rivera@coppel.com', '2020-02-01');
INSERT INTO clientes (nombre, apellido, email, create_at) VALUES('Jesus', 'Mercado', 'jesus.mercado@coppel.com', '2020-02-10');
INSERT INTO clientes (nombre, apellido, email, create_at) VALUES('Ana', 'Martinez', 'ana.martinez@coppel.com', '2020-02-18');
INSERT INTO clientes (nombre, apellido, email, create_at) VALUES('Joel', 'Castaneda', 'joel.castaneda@coppel.com', '2020-02-28');
INSERT INTO clientes (nombre, apellido, email, create_at) VALUES('Leo', 'Rodriguez', 'leo.rodriguez@coppel.com', '2020-03-03');
INSERT INTO clientes (nombre, apellido, email, create_at) VALUES('Rodolfo', 'Tortolero', 'rodolfo.tortolero@coppel.com', '2020-03-04');
INSERT INTO clientes (nombre, apellido, email, create_at) VALUES('Bruno', 'Medina', 'bruno.medina@coppel.com', '2020-03-05');